from django.shortcuts import render, HttpResponse
from .models import *
from django.db.models import Count

# Create your views here.
def index(request):
    
    varGrup1 =Estudiante.objects.filter(grupo=1)
    varGrup4 =Estudiante.objects.filter(grupo=4)
    varApellido= Estudiante.objects.filter(apellidos='Gómez')
    varEdad =Estudiante.objects.filter(edad=22)
    varGrup3= Estudiante.objects.filter(grupo=3,edad=22)
    varAll = Estudiante.objects.all()

    return render(request, 'index.html',{'varGrup1':varGrup1,
                                         'varGrup4':varGrup4,
                                         'varApellido':varApellido,
                                         'varEdad':varEdad,
                                         'varGrup3':varGrup3,
                                         'varAll':varAll})